# Project: Arithmetic Exam Application
https://hyperskill.org/projects/173

## About
Many people are fond of interactive learning. In this project, you will learn how to write an application that can facilitate solving arithmetic operations in a quick manner. The application will generate a mathematical expression for a user to solve. Implement various levels of difficulty and let the application save the results and show the progress of learning.

## Learning outcomes
Learn how to handle input and output, employ the random number generator, and write the output to a file.

## This project is a part of the following tracks
Python Core, Natural Language Processing, Django Developer

## What you’ll do and what you’ll learn
* Stage 1/4: Mini-calculator. Write a mini calculator to work with numbers and simple mathematical operations.
* Stage 2/4: Task generator. Use the random number generator to make your tasks random.
* Stage 3/4: More tasks needed! Test with one question is boring, let's add more questions! Also, we need to find a way to handle user typos.
* Stage 4/4: Adding marks Solving mathematical expressions is helpful, but many students prefer to get marks. Let's add marks and save the results!

## Work on project. Stage 4/4: Adding marks

## Description
Simple tasks are good for younger kids, but math can be more difficult and more interesting! Quadratic equations, trigonometry, and a lot of other interesting things. Math library can help you with that.

Sometimes students want to save the results of the test. This is useful for viewing the learning dynamics on a topic or to identify difficult tasks.

At this stage, let's add integral squares. Of course, you can add more difficulty levels later.

## Objectives
With the first message, the program should ask for a difficulty level:
1 - simple operations with numbers 2-9
2 - integral squares 11-29

A user enters an answer.
For the first difficulty level: the task is a simple math operation; the answer is the result of the operation.
For the second difficulty level: the task is an integer; the answer is the square of this number.
In case of another input: ask to re-enter. Repeat until the format is correct.

The application gives 5 tasks to a user.

The user receives one task, prints the answer.
If the answer contains a typo, print Incorrect format. and ask to re-enter the answer. Repeat until the answer is in the correct format.
If the answer is a number, print Right! or Wrong! Go to the next question.
