import random

operations = {
    "-": lambda a, b: a - b,
    "+": lambda a, b: a + b,
    "*": lambda a, b: a * b,
    "**": lambda a: a ** 2
}
total = 5
tasks = {}
levels = {
    1: "simple operations with numbers 2-9",
    2: "integral squares of 11-29"
}
print("Which level do you want? Enter a number:")
for level, description in levels.items():
    print(f"{level} - {description}")

while True:
    try:
        level = int(input())
        if level in (1, 2):
            break
        else:
            raise ValueError
    except ValueError:
        print("Incorrect format.")

if level == 1:
    for i in range(total):
        task_a, task_operation, task_b = random.randint(2, 9), random.choice(["-", "+", "*"]), random.randint(2, 9)
        task = f"{task_a} {task_operation} {task_b}"
        result = operations[task_operation](task_a, task_b)
        tasks[task] = result
else:
    for i in range(total):
        task_a = random.randint(11, 29)
        result = operations["**"](task_a)
        tasks[f"{task_a}"] = result

right = 0
for task, result in tasks.items():
    print(task)
    while True:
        try:
            user_input = int(input())
            break
        except ValueError:
            print("Wrong format! Try again.")
    if user_input == result:
        print("Right!")
        right += 1
    else:
        print("Wrong!")

print(f"Your mark is {right}/{total}. Would you like to save the result? Enter yes or no.")
save_input = input()
if save_input in ("yes", "YES", "y", "Yes"):
    save_input = True
else:
    save_input = False
if save_input:
    print("What is your name?")
    name = input()
    with open("results.txt", "a") as result_file:
        result_file.write(f"{name}: {right}/{total} in {level} ({levels[level]}).")
    print('The results are saved in "results.txt".')
